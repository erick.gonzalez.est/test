import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroT'
})
export class FiltroTPipe implements PipeTransform {

  transform(arreglo: any[], texto:string, data:string):any[] {
    if(texto ===''){
      return arreglo;
    }

    console.log(arreglo);
    console.log(texto);

    texto=texto.toString().toLowerCase().trim();
    if(data=="TipoServicio"){
      return arreglo.filter(item=>{
        return item.servicio.tipo.nombre.toLowerCase().trim().includes(texto)  ;
     })
    }
    if(data=="Prioridad"){
      return arreglo.filter(item=>{
        return item.servicio.prioridad.nivelPrioridad.toLowerCase().trimp().includes(texto) ;
     })
    }
  }
}

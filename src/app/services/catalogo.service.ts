import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  url = "https://servi-desk.herokuapp.com/catalogo/"
  // url = 'http://192.168.1.50:9898/catalogo/';
  results: Observable<any>;
  constructor(private http: HttpClient) { }

  getCatalogo() {
    return new Promise(resolve => {
      this.http.get(this.url+'listar').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
}
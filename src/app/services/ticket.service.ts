import { Ticket } from 'src/app/models/ticket';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  url = "https://servi-desk.herokuapp.com/ticket/"
  // url = 'http://192.168.1.50:9898/ticket/';
  // url="http://localhost:9898/ticket/";
  ticketEstado: any = true;
  confirmacionEstado: any = false;
  results: Observable<any>;
  constructor(private http: HttpClient) { }

  dataTicket(codTecnico:number ): Observable<any>{
    return this.http.get(`${this.url}tecnico/`+ codTecnico);
  }

  // getTicketsByUser(codUsuario:number ): Observable<any>{
  //   return this.http.get(`${this.url}usuario/`+ codUsuario);
  // }

  getTicketsByUser(codUsuario:number) {
    return new Promise(resolve => {
      this.http.get(this.url+'usuario/'+codUsuario).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getTicket() {
    return new Promise(resolve => {
      this.http.get(this.url+'listar').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getTicketById(id:number) {
    return new Promise(resolve => {
      this.http.get(this.url+'listar/'+id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  //http://localhost:9898/ticket/asignarTecnico/1/1
  putAsignarTecnico(cod_tecnico:number,codCoordinador:number,cod_ticket:number){

    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers};
      this.http.put(this.url+'asignarTecnico/'+cod_tecnico+'/'+codCoordinador+'/'+cod_ticket,null,options).subscribe(data => {
        resolve(data);
      }, err => {

        console.log(err);
      });
    });
  }

  putCambiarEstado(cod_estado:number, cod_ticket: number){
    return new Promise(resolve =>{
      let headers = new HttpHeaders({ 'Content-Type':'application/json'});
      let options = {headers: headers};
      this.http.put(this.url+'cambiar_estado/'+cod_estado+'/'+cod_ticket,null,options).subscribe(data =>{
        resolve(data);
      },err =>{
        console.log(err);
      });
    });
  }
  putFechaAsignacion(cod_tecnico:number,ticket:Ticket){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers};
      this.http.put(this.url+'fecha_asignacion/'+cod_tecnico,ticket,options).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  async createTicket(ticket: Ticket) {
    const headers = { "content-type": "application/json" };
    let body = {
      "descripcionTicket": ticket.descripcionTicket.toString(),
      "estado": {
        "codEstado": ticket.estado
      },
      "fechaCreacion": ticket.fechaCreacion.toString(),
      "servicio": {
        "codServicio": ticket.codServicio
      },
      "severidad": {
        "codSeveridad": ticket.codSeveridad
      },
      "confirmacion": this.confirmacionEstado,
      "ticketEstado": this.ticketEstado,
      "url": ticket.url.toString(),
      "sla": ticket.sla,
      "usuario": {
        "codUsuario": ticket.codUsuario
      },
      "viaComunicacion": {
        "codViaComunicacion": ticket.codViaComunicacion
      }
    }
    return this.http
      .post(this.url+"guardar", body, {
        headers: headers,
        observe: "response"
      })
      .toPromise();
  }

  async updateTicket(ticket: Ticket) {
    console.log("ticket a actualizar "+JSON.stringify(ticket))
    const headers = { "content-type": "application/json" };
    let body = {
      "codticket": ticket.codTicket,
      "descripcionTicket": ticket.descripcionTicket.toString(),
      "estado": {
        "codEstado": ticket.estado
      },
      "fechaCreacion": ticket.fechaCreacion.toString(),
      "servicio": {
        "codServicio": ticket.codServicio
      },
      "severidad": {
        "codSeveridad": ticket.codSeveridad
      },
      "ticketEstado": this.ticketEstado,
      "url": ticket.url.toString(),
      "sla": ticket.sla,
      "usuario": {
        "codUsuario": ticket.codUsuario
      },
      "viaComunicacion": {
        "codViaComunicacion": ticket.codViaComunicacion
      }
    }
    return this.http
      .post(this.url+"guardar", body, {
        headers: headers,
        observe: "response"
      })
      .toPromise();
  }

  getTicketByTipoServicio(tipo:string) {
    return new Promise(resolve => {
      this.http.get(this.url+'tipo_servicio/'+tipo).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  putReasignar(codticket:any){
    return new Promise(resolve =>{
      let headers = new HttpHeaders({'Content-Type':'application/json'});
      let options = {headers:headers};
      this.http.put(this.url+'reasignar/'+codticket,options).subscribe(data =>{
        resolve(data);
      },err =>{
        console.log(err);
      });
    });
  }
  CambioDeEstadoTicket(id:number,estado:boolean ){
    return new Promise(resolve => {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers};
      this.http.put(this.url+'eliminar_recuperar/'+estado+'/'+id,null,options).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getTicketByEliminados_Activos(booleano:boolean) {
    return new Promise(resolve => {
      this.http.get(this.url+'listar_eliminados/'+booleano).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getHistorialEscalar(codticket){
    const urls='https://servi-desk.herokuapp.com/historialEscalar/buscarByTicket/';
    return new Promise(resolve => {
      this.http.get(urls+codticket).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  putConfirmar(codTicket:any){
  return new Promise(resolve =>{
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    let options = { headers: headers};
    this.http.put(this.url+'confirmar/'+codTicket, null, options).subscribe(data =>{
      resolve(data);
    },err =>{
        console.log(err);
      });
    });
  }
  //http://localhost:9898/ticket/tipo_servicio/

}

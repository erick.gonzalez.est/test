import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Historial } from 'src/app/models/historial';

@Injectable({
  providedIn: 'root'
})
export class HistorialEscalarService {
  //url = 'http://localhost:9898/historialEscalar/'
  url = 'https://servi-desk.herokuapp.com/historialEscalar/';
  constructor(private http:HttpClient) { }

  setHistorial(historial:Historial){
    console.log(historial);
    return new Promise(resolve =>{
      let headers = new HttpHeaders({'Content-Type': 'application/json' });
      let options = { headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin' : "*"
      })};
      this.http.post(this.url+'guardar',historial).subscribe(data =>{
        resolve(data);
      }, err =>{
        console.log(err);
      });
    })
  }

  getById(codticket:any){
    return new Promise(resolve =>{
      this.http.get(this.url+'buscarByTicket/'+codticket).subscribe(data =>{
        resolve(data);
      },err =>{
        console.log(err);
      });
    });
  }

}

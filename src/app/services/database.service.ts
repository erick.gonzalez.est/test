import { Injectable } from '@angular/core';
import { SQLiteObject,SQLite } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private database: SQLiteObject;
  private dbReady = new BehaviorSubject<boolean>(false);
  constructor(private sqlite:SQLite) { 
    
  }
  crearDataBase(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.database = db;
        this.crearTablas().then(()=>{     
          
          alert("si")
          this.dbReady.next(true);
        });
        this.getaddTicket().then(()=>{    
          alert("guardado") 
         
          this.dbReady.next(true);
        });
    
      })
      .catch(e => console.log(e));
  }
  
  crearTablas(){
    return this.database.executeSql(
    `CREATE TABLE  IF NOT EXISTS ticket (
      codticket  INTEGER PRIMARY KEY AUTOINCREMENT,
       descripcion_ticket character varying(255),
      fecha_asignacion timestamp without time zone,
      fecha_creacion timestamp without time zone,
      sla double precision ,
      url character varying(255),
      cod_coordinador bigint,
      cod_estado bigint,
      cod_servicio bigint NOT NULL,
      cod_severidad bigint,
      cod_tecnico bigint,
      cod_usuario bigint NOT NULL,
      cod_via_comunicacion bigint NOT NULL
  )`,[]).then().catch((err)=>alert("error detected creating tables"+ err));
  }
  getaddTicket(){
    return this.database.executeSql( `INSERT INTO public.ticket(
      codticket, fecha_asignacion, fecha_creacion, sla, url, cod_coordinador, cod_estado, cod_servicio,  cod_tecnico, cod_usuario, cod_via_comunicacion)
      VALUES (1, '2020-12-09', '2020-12-10', 36.3, 'url', 1, null, 1,  null, 1, 1);`,[] )
  }
  getTicket(){
      return this.database.executeSql("SELECT * from ticket", [])
      .then((data)=>{
        let lists = [];
        for(let i=0; i<data.rows.length; i++){
          lists.push(data.rows.item(i));
        }
        return lists;
      })
  }
  addTickets( name:string){
    this.database.executeSql(`INSERT INTO ticket(name) VALUES ('${name}');`, []).then((result)=>{
      if(result.insertId){
        return this.getTicketForId(result.insertId);
      }
    })
  }
  getTicketForId(id:number){
    this.database.executeSql(`SELECT * FROM ticket WHERE codticket = ${id}`, [])
  }

  deleteList(id:number){
    this.database.executeSql(`DELETE FROM ticket WHERE codticket = ${id}`, [])
  }
}

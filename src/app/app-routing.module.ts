import { LoginPage } from './pages/login/login.page';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

let logued = localStorage.getItem('logued');
let tipo = localStorage.getItem('TipoUsuario');
let root = 'login';

console.log("Is logued? "+ logued);


if (logued != null && tipo == 'Usuario') {
  root = 'lista-ticket-usuario';
}

if (logued != null && tipo == 'Tecnico') {
  root = 'lista-tecnico';
}

if (logued != null && tipo == 'Coordinador') {
  root = 'lista-ticket-coordinador/:id';
}

const routes: Routes = [
  {
    path: '',
    redirectTo: root,
    pathMatch: 'full',
  },
  {
    path: 'registro',
    loadChildren: () =>
      import('./pages/registro/registro.module').then(
        (m) => m.RegistroPageModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'registro-ticket',
    loadChildren: () =>
      import('./pages/registro-ticket/registro-ticket.module').then(
        (m) => m.RegistroTicketPageModule
      ),
  },
  {
    path: 'lista-tecnico',
    loadChildren: () =>
      import('./pages/lista-tecnico/lista-tecnico.module').then(
        (m) => m.ListaTecnicoPageModule
      ),
  },
  {
    path: 'lista-ticket-coordinador/:id',
    loadChildren: () =>
      import(
        './pages/lista-ticket-coordinador/lista-ticket-coordinador.module'
      ).then((m) => m.ListaTicketCoordinadorPageModule),
  },
  {
    path: 'detalle-ticket-coordinador/:id',
    loadChildren: () =>
      import(
        './pages/detalle-ticket-coordinador/detalle-ticket-coordinador.module'
      ).then((m) => m.DetalleTicketCoordinadorPageModule),
  },
  {
    path: 'modal-lista-tecnico',
    loadChildren: () =>
      import('./pages/modal-lista-tecnico/modal-lista-tecnico.module').then(
        (m) => m.ModalListaTecnicoPageModule
      ),
  },
  {
    path: 'lista-tecnicos-coordinar-asignar',
    loadChildren: () =>
      import(
        './pages/lista-tecnicos-coordinar-asignar/lista-tecnicos-coordinar-asignar.module'
      ).then((m) => m.ListaTecnicosCoordinarAsignarPageModule),
  },
  {
    path: 'lista-ticket-usuario',
    loadChildren: () =>
      import('./pages/lista-ticket-usuario/lista-ticket-usuario.module').then(
        (m) => m.ListaTicketUsuarioPageModule
      ),
  },
  {
    path: 'detalle-ticket-tecnico',
    loadChildren: () =>
      import(
        './pages/detalle-ticket-tecnico/detalle-ticket-tecnico.module'
      ).then((m) => m.DetalleTicketTecnicoPageModule),
  },
  {
    path: 'solucion',
    loadChildren: () =>
      import('./pages/solucion/solucion.module').then(
        (m) => m.SolucionPageModule
      ),
  },
  {
    path: 'tickets-eliminados-coordinador',
    loadChildren: () =>
      import(
        './pages/tickets-eliminados-coordinador/tickets-eliminados-coordinador.module'
      ).then((m) => m.TicketsEliminadosCoordinadorPageModule),
  },
  {
    path: 'tickets-rechazados-coordinador',
    loadChildren: () =>
      import(
        './pages/tickets-rechazados-coordinador/tickets-rechazados-coordinador.module'
      ).then((m) => m.TicketsRechazadosCoordinadorPageModule),
  },
  {
    path: 'detalle-ticket-usuario',
    loadChildren: () =>
      import(
        './pages/detalle-ticket-usuario/detalle-ticket-usuario.module'
      ).then((m) => m.DetalleTicketUsuarioPageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { Tipo } from './tipo';
import { prioridad } from './prioridad';
import { Catalogo } from './catalogos';
export interface Servicio{
    codServicio:number;
    descripcion:string;
    titulo:string;
    viaComunicacion:string;
    fechaServicio:Date;
    catalogo:Catalogo;
    prioridad:prioridad;
    tipo:Tipo
}
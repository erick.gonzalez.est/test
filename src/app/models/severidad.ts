export class Severidad {
    codSeveridad: number;
    nivelSeveridad: string;
    descripcionSeveridad: string;
    valorSeveridad: number;
}

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListaTecnicosCoordinarAsignarPage } from './lista-tecnicos-coordinar-asignar.page';

describe('ListaTecnicosCoordinarAsignarPage', () => {
  let component: ListaTecnicosCoordinarAsignarPage;
  let fixture: ComponentFixture<ListaTecnicosCoordinarAsignarPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTecnicosCoordinarAsignarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaTecnicosCoordinarAsignarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

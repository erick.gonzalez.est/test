import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaTecnicosCoordinarAsignarPage } from './lista-tecnicos-coordinar-asignar.page';

const routes: Routes = [
  {
    path: '',
    component: ListaTecnicosCoordinarAsignarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaTecnicosCoordinarAsignarPageRoutingModule {}

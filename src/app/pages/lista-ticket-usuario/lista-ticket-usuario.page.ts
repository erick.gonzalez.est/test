import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, NavController} from '@ionic/angular';
import { Ticket } from 'src/app/models/ticket';
import { ServicioService } from 'src/app/services/servicio.service';
import { TicketService } from 'src/app/services/ticket.service';
import { UtilsService } from 'src/app/utils/utils.service';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-lista-ticket-usuario',
  templateUrl: './lista-ticket-usuario.page.html',
  styleUrls: ['./lista-ticket-usuario.page.scss'],
})
export class ListaTicketUsuarioPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  // ticket: Ticket[] = [];
  ticket: any=[];
  ticketFilter: any[];
  usuario: any;
  validate: boolean = false;
  titulo: String = 'Tickets';
  textoBusqueda:string='';
  tipoServicio:string='codTicket';
  defectSelect:string="Buscar tickets";
  fechaSelected: any = 1;
  fecha: string = new Date().toISOString();
  codTipo: any;

  constructor(
    private ticketService: TicketService,
    private servicioService: ServicioService,
    private utils: UtilsService,
    private router: Router,
    private navCtrl: NavController,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    console.log("codigo de usuario desde login");
    this.usuario = parseInt(localStorage.getItem("idUsuario"));
    console.log(this.usuario);
  }

  ionViewWillEnter(){
    this.listarTickets();
    localStorage.removeItem('codTicket');
    localStorage.removeItem('editable');
    localStorage.removeItem('ticket');
  }
  

  doRefresh(event: { target: { complete: () => void; }; }) {
    setTimeout(() => {
      this.listarTickets();
      event.target.complete();
    }, 2000);
  }
  
  buscar(event: { detail: { value: any; }; }){
    this.textoBusqueda=event.detail.value;
  }

  buscarFecha(event: { detail: { value: any; }; }){
    var dateFormat = event.detail.value.split('T')[0]; 
    console.log(dateFormat);
    this.textoBusqueda=dateFormat;
  }

  selectTipoServicio(codTipo: any) {
    if (codTipo === 1) {
      localStorage.setItem("codTipo", codTipo);
      localStorage.setItem("tituloTipo", "Incidente");
      console.log("codigo de tipo seleccionado "+codTipo);
    } else {
      localStorage.setItem("codTipo", codTipo);
      localStorage.setItem("tituloTipo", "Requerimiento");
      console.log("codigo de tipo seleccionado "+codTipo);
    }
    this.router.navigateByUrl('/registro-ticket');
  }

  listarTickets() {
    this.ticketService.getTicketsByUser(this.usuario).then(data => {
      this.ticket = data;
      if (this.ticket.length == 0) {
        this.utils.presentAlert('No tiene tickets registrados');
      }
    },
    (error) => {
      console.log(error);
    });
  }

  detalle(ticket:any){
    localStorage.setItem('codTicket',ticket.codticket)
    this.router.navigateByUrl("/detalle-ticket-usuario");
    // this.navCtrl.navigateForward('detalle-ticket-usuario');
  }

  async filtroButton() {
    const alert = await this.alertCtrl.create({
      header: 'Metodo de busqueda: ',
      inputs: [
        {
          type: 'radio',
          label: 'Ticket',
          value: 'codTicket'
        },
        {
          type: 'radio',
          label: 'Categoria',
          value: 'Categoria'
        },
        {
          type: 'radio',
          label: 'Servicio',
          value: 'Servicio'
        },
        {
          type: 'radio',
          label: 'Fecha',
          value: 'Fecha'
        },
        {
          type: 'radio',
          label: 'Estado',
          value: 'Estado'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: (data: any) => {
            
          }
        },
        {
          text: 'Listo',
          handler: (data: any) => {
            this.textoBusqueda='';
            if(data==="codTicket"){
              this.tipoServicio="codTicket";
              this.defectSelect="Ingrese un codigo";
              this.fechaSelected = 1;
            }
            if(data==="Categoria"){
              this.tipoServicio="Categoria";
              this.defectSelect="Ingrese una categoria";
              this.fechaSelected = 1;
            }
            if(data==="Servicio"){
              this.tipoServicio='Servicio';
              this.defectSelect="Ingrese un servicio";
              this.fechaSelected = 1;
            } 
            if(data==="Fecha"){
              this.tipoServicio='Fecha';
              this.defectSelect="Ingrese una fecha";
              this.fechaSelected = 2;
            }
            if(data==="Estado"){
              this.tipoServicio='Estado';
              this.defectSelect="Ingrese un estado";
              this.fechaSelected = 1;
            }
          }
        }
      ]
    });
    await alert.present();
  }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.ticket.length == 5) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}

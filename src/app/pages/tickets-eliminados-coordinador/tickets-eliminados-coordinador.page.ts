import { UtilsService } from './../../utils/utils.service';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { TicketService } from 'src/app/services/ticket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tickets-eliminados-coordinador',
  templateUrl: './tickets-eliminados-coordinador.page.html',
  styleUrls: ['./tickets-eliminados-coordinador.page.scss'],
})
export class TicketsEliminadosCoordinadorPage implements OnInit {
  eliminados:any=[];
  estadoTicket:string;
  textoBusqueda:string='';
  tipoServicio:string='Servicio';
  defectSelect:string="Buscar por: Servicio";
  constructor(private loadingController:LoadingController,private utils:UtilsService,private servicioTicket:TicketService,private alertController:AlertController,private navCtrl: NavController, private toast:UtilsService) { 
    this.estadoTicket="Incidentes";
  }

  ngOnInit() {
    this.descargandoDatos()
  }
  buscar(event){
    this.textoBusqueda=event.detail.value;
  }
  
  async descargandoDatos(){
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Descargando datos...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    setTimeout(() => {
      this.ConsultaDeRickets();
    }, 1000);
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }
  doRefresh(event) {
    this.ngOnInit();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 500);
  }
  ConsultaDeRickets(){
    this.servicioTicket.getTicketByEliminados_Activos(false).then(data=>{
      console.log(data);
      this.eliminados=data;
      if(this.eliminados.length<=0){
        this.utils.presentToastLenin("No se encontraron datos.");
      }
    });
  }
  segmentChanged(ev: any) {
    this.estadoTicket=ev.detail.value;
  }
  AbrirTicket(ticket:any){
    this.navCtrl.navigateForward('/detalle-ticket-coordinador/'+ticket.codticket);
  }
  async filtroButton() {
    const alert = await this.alertController.create({
      header: 'Configuracion de barra de busqueda',
      subHeader: 'Seleccione el modo de busqueda:',
      inputs: [
        {
          type: 'radio',
          label: 'Nombre de usuario',
          value: 'usuario'
        },
        {
          type: 'radio',
          label: 'Tecnico',
          value: 'tecnico'
        },
        {
          type: 'radio',
          label: 'Nivel de Criticidad',
          value: 'criticidad'
        },
        {
          type: 'radio',
          label: 'Servicio',
          value: 'Servicio'
        },
        {
          type: 'radio',
          label: 'Tipo de Severidad',
          value: 'severidad'
        },
        {
          type: 'radio',
          label: 'Nivel de prioridad',
          value: 'prioridad'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            
          }
        },
        {
          text: 'Ok',
          handler: (data: any) => {
            
            if(data=="usuario"){
              this.tipoServicio="usuario";
              this.defectSelect="Buscar por: Nombre de usuario";
            }
            if(data=="tecnico"){
              this.tipoServicio='tecnico';
              this.defectSelect="Buscar por: tecnico";
            } 
            if(data=="TipoServicio"){
              this.tipoServicio='TipoServicio';
              this.defectSelect="Buscar por: Tipo de Servicio";
            }
            if(data=="criticidad"){
              this.tipoServicio='criticidad';
              this.defectSelect="Buscar por: Nivel de criticidad";
            }
            if(data=="Servicio"){
              this.tipoServicio='Servicio';
              this.defectSelect="Buscar por: Servicio";
            }
            if(data=="severidad"){
              this.tipoServicio='severidad';
              this.defectSelect="Buscar por: Nivel de Severidad";
            }
            if(data=="prioridad"){
              this.tipoServicio='prioridad';
              this.defectSelect="Buscar por: Nivel de prioridad";
            }
          }
        }
      ]
    });
    await alert.present();
  }
  async RecuperarTicket(id:number){
    const alert = await this.alertController.create({
      header: 'Recuperar ticket',
      message: '¿Está seguro que quiere RECUPERAR este ticket? ',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'RECUPERAR',
          handler: () => {
            this.servicioTicket.CambioDeEstadoTicket(id,true).then(data=>{
              this.toast.presentToastLenin("Ticket Recuperado, refresque para ver cambios");
            });
            this.ConsultaDeRickets();
          }
        }
      ]
    });
    await alert.present();
  }

}

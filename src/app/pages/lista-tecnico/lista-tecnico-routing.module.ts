import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaTecnicoPage } from './lista-tecnico.page';

const routes: Routes = [
  {
    path: '',
    component: ListaTecnicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaTecnicoPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleTicketUsuarioPageRoutingModule } from './detalle-ticket-usuario-routing.module';

import { DetalleTicketUsuarioPage } from './detalle-ticket-usuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleTicketUsuarioPageRoutingModule
  ],
  declarations: [DetalleTicketUsuarioPage]
})
export class DetalleTicketUsuarioPageModule {}

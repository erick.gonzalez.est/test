import { UtilsService } from 'src/app/utils/utils.service';
import { ActivatedRoute } from '@angular/router';
import { TicketService } from './../../services/ticket.service';
import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, MenuController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-lista-ticket-coordinador',
  templateUrl: './lista-ticket-coordinador.page.html',
  styleUrls: ['./lista-ticket-coordinador.page.scss'],
})
export class ListaTicketCoordinadorPage implements OnInit {
  estadoTicket:string;
  ticket: any=[];
  textoBusqueda:string='';
  tipoServicio:string='Servicio';
  defectSelect:string="Buscar por: Servicio";
  consultaServicio:string;
  constructor(private loadingController:LoadingController,private utils:UtilsService,private route: ActivatedRoute,private menu: MenuController,private alertController:AlertController,private servicio:TicketService, private navCtrl: NavController) { 
    this.estadoTicket="Sin Asignar";
  }
  doRefresh(event) {
    this.ngOnInit();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 500);
  }
  async EliminarTicket(id:number){
    const alert = await this.alertController.create({
      header: 'Eliminar ticket',
      message: '¿Está seguro que quiere ELIMINAR este ticket? ',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Eliminar',
          handler: () => {
            this.servicio.CambioDeEstadoTicket(id,false).then(data=>{
              this.utils.presentToastLenin("Ticket eliminado, Deslice para refrescar")
            }).catch(data=>{
              this.utils.presentToastLenin("Ticket no eliminado")
            });
          }
        }
      ]
    });
    await alert.present();
  }
  ngOnInit() {
    this.descargandoDatos()
  }
  cargarDatos(){
    this.consultaServicio = this.route.snapshot.paramMap.get('id');
    this.servicio.getTicketByTipoServicio(this.consultaServicio).then((data)=>{
      this.ticket=data;
      console.log(data,this.consultaServicio);
    }).catch(data=>{
      console.log(data)
    })
  }
  async descargandoDatos(){
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: 'Descargando datos...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    setTimeout(() => {
      this.cargarDatos();
    }, 1000);
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }
  buscar(event){
    this.textoBusqueda=event.detail.value;
  }
  getTickets() {
    this.servicio.getTicket().then(data => {
      if(data!=null){
        this.ticket = data;
      }else{
        this.utils.presentToastLenin("No se encontraron datos.");
      }
    });
  }
  segmentChanged(ev: any) {
    this.estadoTicket=ev.detail.value;
  }
  asignar_tecnico(ticket:any){
    this.navCtrl.navigateForward('/detalle-ticket-coordinador/'+ticket.codticket);
    console.log(ticket )
  }
  async filtroButton() {
    const alert = await this.alertController.create({
      header: 'Configuracion de barra de busqueda',
      subHeader: 'Seleccione el modo de busqueda:',
      inputs: [
        {
          type: 'radio',
          label: 'Nombre de usuario',
          value: 'usuario'
        },
        {
          type: 'radio',
          label: 'Tecnico',
          value: 'tecnico'
        },
        {
          type: 'radio',
          label: 'Nivel de Criticidad',
          value: 'criticidad'
        },
        {
          type: 'radio',
          label: 'Servicio',
          value: 'Servicio'
        },
        {
          type: 'radio',
          label: 'Tipo de Severidad',
          value: 'severidad'
        },
        {
          type: 'radio',
          label: 'Nivel de prioridad',
          value: 'prioridad'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            
          }
        },
        {
          text: 'Ok',
          handler: (data: any) => {
            
            if(data=="usuario"){
              this.tipoServicio="usuario";
              this.defectSelect="Buscar por: Nombre de usuario";
            }
            if(data=="tecnico"){
              this.tipoServicio='tecnico';
              this.defectSelect="Buscar por: tecnico";
            } 
            if(data=="TipoServicio"){
              this.tipoServicio='TipoServicio';
              this.defectSelect="Buscar por: Tipo de Servicio";
            }
            if(data=="criticidad"){
              this.tipoServicio='criticidad';
              this.defectSelect="Buscar por: Nivel de criticidad";
            }
            if(data=="Servicio"){
              this.tipoServicio='Servicio';
              this.defectSelect="Buscar por: Servicio";
            }
            if(data=="severidad"){
              this.tipoServicio='severidad';
              this.defectSelect="Buscar por: Nivel de Severidad";
            }
            if(data=="prioridad"){
              this.tipoServicio='prioridad';
              this.defectSelect="Buscar por: Nivel de prioridad";
            }
          }
        }
      ]
    });
    await alert.present();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalListaTecnicoPageRoutingModule } from './modal-lista-tecnico-routing.module';

import { ModalListaTecnicoPage } from './modal-lista-tecnico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalListaTecnicoPageRoutingModule
  ],
  declarations: [ModalListaTecnicoPage]
})
export class ModalListaTecnicoPageModule {}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { NavController, Platform } from '@ionic/angular';
import { Catalogo } from 'src/app/models/catalogos';
import { Servicio } from 'src/app/models/servicio';
import { Severidad } from 'src/app/models/severidad';
import { Ticket } from 'src/app/models/ticket';
import { ViaComunicacion } from 'src/app/models/via-comunicacion';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { ConsumoImagenService } from 'src/app/services/consumo-imagen.service';
import { ServicioService } from 'src/app/services/servicio.service';
import { SeveridadService } from 'src/app/services/severidad.service';
import { TicketService } from 'src/app/services/ticket.service';
import { ViaComunicacionService } from 'src/app/services/via-comunicacion.service';
import { UtilsService } from 'src/app/utils/utils.service';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-registro-ticket',
  templateUrl: './registro-ticket.page.html',
  styleUrls: ['./registro-ticket.page.scss'],
})
export class RegistroTicketPage implements OnInit {
  fileObj: ChooserResult;
  uploadText: any;
  downloadText: any;
  dataServicio: Servicio[] = [];
  dataCatalogo: Catalogo[] = [];
  dataSeveridad: Severidad[] = [];
  dataViaComunicacion: ViaComunicacion[] = [];
  codUsuario: any;
  fecha: string = new Date().toISOString();
  sendMsg: any = '';
  urlChange: any;

  // from lista tickets
  titulo: string;
  codTipo: any;

  //ng Variables
  codCatalogo: any;
  tituloCatalogo: any;
  codServicio: any;
  tituloServicio: any;
  codSeveridad: any;
  nivelSeveridad: any;
  descripcionTicket: any;
  codViaComunicacion: any;
  nombreViaComunicacion: any;
  imgUrl: any;
  codTicket: any;
  editable: any;
  fileName: any;

    //datos para clacular el SLA
    valorCritividad: any;
    valorPrioridad: any;
    valorSevridad: any;
    obtenercodigoTicket: any;
  

  archivo: File;
  photoSelected: string | ArrayBuffer;
  processing: boolean;
  uploadImage: string;
  img: String;

  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private utils: UtilsService,
    private consumo: ConsumoImagenService,
    private servicioService: ServicioService,
    private catalogoService: CatalogoService,
    private severidadService: SeveridadService,
    private viaComunicacionService: ViaComunicacionService,
    private ticketService: TicketService,
    private router: Router,
    public navCtrl: NavController,
    private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient
  ) {
    this.plt.ready().then(() => {
      this.sqlite
        .create({
          name: 'tickets.db',
          location: 'default',
        })
        .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
        });
    });
  }

  seedDatabase() {
    this.http
      .get('assets/seed.sql', { responseType: 'text' })
      .subscribe((sql) => {
        this.sqlitePorter
          .importSqlToDb(this.database, sql)
          .then((_) => {
            this.dbReady.next(true);
          })
          .catch((e) => console.error(e));
      });
  }


  updateTicket() {
    let path =
      "UPDATE ticket SET codCatalogo=?, codServicio=?, codSeveridad=?, descripcionTicke=?t, codViaComunicacion=?, codUsuario=?, url=?, fechaCreacion=?, estado=?, confirmacion=? , ticketEstado=? WHERE codTicke=?)";
    try {
      const data = this.database.executeSql(path, [
        this.codTicket,
        this.codCatalogo,
        this.codServicio,
        this.descripcionTicket,
        this.codSeveridad,
        this.codViaComunicacion,
        this.codUsuario,
        this.imgUrl,
        this.fecha,
        1,
        false,
        true,
      ]);
      console.log(
        'ticket actualizado en BD con codigo: ' + this.codTicket + ' :D'
      );
    } catch (err) {
      console.log('Registrar Error ' + err.error);
    }
  }

  addTicket() {
    const data = [
      this.codTicket,
      this.codCatalogo,
      this.codServicio,
      this.descripcionTicket,
      this.codSeveridad,
      this.codViaComunicacion,
      this.codUsuario,
      this.imgUrl,
      this.fecha,
      1,
      false,
      true,
    ];

    try {
      const data_1 = this.database.executeSql(
        "INSERT INTO ticket ('codTicket, codCatalogo, codServicio, codSeveridad, descripcionTicket, codViaComunicacion, codUsuario, url, fechaCreacion, estado, confirmacion , ticketEstado') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
        data
      );
      console.log(
        ' ticket registrado en BD con codigo: ' + this.codTicket + ' :D'
      );
    } catch (err) {
      console.log('Registrar Error ' + err.error);
    }
  }

  async ngOnInit() {
    this.getCatalogos();
    this.getSeveridad();
    this.getViaComunicacion();
    this.codTipo = localStorage.getItem('codTipo');
    this.titulo = localStorage.getItem('tituloTipo');
    console.log(
      ' al iniciar se traen estos datos: ' + this.codTipo,
      this.titulo
    );

    this.editable = localStorage.getItem('editable');
    console.log('el registro es editable? ' + this.editable);
    if (this.editable === '1') {
      this.codTicket = localStorage.getItem('codTicket');
      console.log('existe un codigo? ' + this.codTicket);
      await this.getTicketById(this.codTicket);
      // this.getServicios(this.codTipo, this.codCatalogo);
    }
    console.log('url al cargar arriba: ', this.imgUrl);
    this.codUsuario = parseInt(localStorage.getItem('idUsuario'));
  }

  ionViewDidLeave() {
    localStorage.removeItem('codCatalogo');
    localStorage.removeItem('tituloCatalogo');
    localStorage.removeItem('codServicio');
    localStorage.removeItem('tituloServicio');
    localStorage.removeItem('codSeveridad');
    localStorage.removeItem('nivelSeveridad');
    localStorage.removeItem('codSeveridad');
    localStorage.removeItem('nivelSeveridad');
    localStorage.removeItem('codViaComunicacion');
    localStorage.removeItem('nombreViaComunicacion');
    localStorage.removeItem('url');
    localStorage.removeItem('codTipo');
    localStorage.removeItem('tituloTipo');
    localStorage.removeItem('ticket');
    console.log('si esta borrando cosas :D');
  }

  async getTicketById(codTicket: any) {
    this.ticketService.getTicketById(codTicket).then((data) => {
      this.codCatalogo = data[0].servicio.catalogo.codCatalogo;
      this.tituloCatalogo = data[0].servicio.catalogo.categoria;
      this.codServicio = data[0].servicio.codServicio;
      this.tituloServicio = data[0].servicio.titulo;
      this.codSeveridad = data[0].severidad.codSeveridad;
      this.nivelSeveridad = data[0].severidad.nivelSeveridad;
      this.descripcionTicket = data[0].descripcionTicket;
      this.codViaComunicacion = data[0].viaComunicacion.codViaComunicacion;
      this.nombreViaComunicacion = data[0].viaComunicacion.nombre;
      this.imgUrl = data[0].url;
      console.log('url al cargar abajo: ', this.imgUrl);
      this.codTipo = data[0].servicio.tipo.codTipo;
      this.titulo = data[0].servicio.tipo.nombre;
      this.getServicios(this.codTipo, this.codCatalogo);
    });
  }

  popView() {
    this.router.navigateByUrl('/lista-ticket-usuario');
  }

  popViewMod() {
    this.router.navigateByUrl('/detalle-ticket-usuario');
  }

  async uploadFile() {
    this.consumo.onClickSubir();
    setTimeout(async () => {
      this.imgUrl = localStorage.getItem('url');
      console.log('url de imagen ' + this.imgUrl);
    }, 3000);
  }

  onChange(fileChangeEvent: { target: { files: File[] } }) {
    this.archivo = fileChangeEvent.target.files[0];
    this.imgUrl = '';
    this.fileName = this.archivo.name;
    console.log(this.fileName);
    this.consumo.onChange(this.archivo);
    this.urlChange = 1;
  }

  async uploadTicket() {
    //   console.log("servidor on? =", this.isServerAvailable);
    //   if (this.isServerAvailable) {
    await this.uploadFile();
    this.utils.presentLoading('Guardando');
    setTimeout(async () => {
      await this.createTicket();
      this.utils.presentToastLenin(this.sendMsg);
      this.popView();
    }, 3500);
    localStorage.removeItem('url');
    //   }
  }

  async modificarTicket() {
    //   console.log("servidor on? =", this.isServerAvailable);
    //   if (this.isServerAvailable) {
    console.log('test antes de if modificar: ', this.imgUrl);
    if (this.urlChange != null) {
      console.log('test despues de if modificar: ', this.imgUrl);
      await this.uploadFile();
    }
    this.utils.presentLoading('Guardando');
    setTimeout(async () => {
      await this.createTicket();
      this.utils.presentToastLenin(this.sendMsg);
      this.popViewMod();
    }, 3500);
    localStorage.removeItem('url');
    //   }
  }

  async onChangeCatalogo(event: { target: { value: any } }) {
    localStorage.removeItem('tituloServicio');
    this.tituloServicio = '';
    localStorage.removeItem('codServicio');
    localStorage.setItem('codCatalogo', event.target.value);
    let codCatalogo = localStorage.getItem('codCatalogo');
    this.getServicios(this.codTipo, codCatalogo);
    console.log('codigo de catalogo seleccionado/guardado ' + codCatalogo);
    for (const data of this.dataCatalogo) {
      if (codCatalogo === data.codCatalogo.toString()) {
        localStorage.setItem('tituloCatalogo', data.categoria);
        this.tituloCatalogo = localStorage.getItem('tituloCatalogo');
      } else {
        this.tituloCatalogo = data.categoria;
      }
    }
  }

  async onChangeServicio(event: { target: { value: any } }) {
    localStorage.setItem('codServicio', event.target.value);
    let codServicio = localStorage.getItem('codServicio');
    console.log('codigo de servicio seleccionado/guardado ' + codServicio);
    for (const data of this.dataServicio) {
      if (codServicio === data.codServicio.toString()) {
        localStorage.setItem('tituloServicio', data.titulo);
        this.tituloServicio = localStorage.getItem('tituloServicio');
      } else {
        this.tituloServicio = data.titulo;
      }
    }
  }

  async onChangeSeveridad(event: { target: { value: any } }) {
    localStorage.setItem('codSeveridad', event.target.value);
    let codSeveridad = localStorage.getItem('codSeveridad');
    console.log('codigo de severidad seleccionado/guardado ' + codSeveridad);
    for (const data of this.dataSeveridad) {
      if (codSeveridad === data.codSeveridad.toString()) {
        localStorage.setItem('nivelSeveridad', data.nivelSeveridad);
        this.nivelSeveridad = localStorage.getItem('nivelSeveridad');
      } else {
        this.nivelSeveridad = data.nivelSeveridad;
      }
    }
  }

  async onChangeViaComunicacion(event: { target: { value: any } }) {
    localStorage.setItem('codViaComunicacion', event.target.value);
    let codViaComunicacion = localStorage.getItem('codViaComunicacion');
    console.log(
      'codigo de viaComunicacion seleccionado/guardado ' + codViaComunicacion
    );
    for (const data of this.dataViaComunicacion) {
      if (codViaComunicacion === data.codViaComunicacion.toString()) {
        localStorage.setItem('nombreViaComunicacion', data.nombre);
        this.nombreViaComunicacion = localStorage.getItem(
          'nombreViaComunicacion'
        );
      } else {
        this.nombreViaComunicacion = data.nombre;
      }
    }
  }

  /////////////////////////////////////////////////////////////

  getViaComunicacion() {
    this.viaComunicacionService.getViaComunicacion().then((data: any) => {
      this.dataViaComunicacion = data;
    });
  }

  getSeveridad() {
    this.severidadService.getListSeveridad().then((data: any) => {
      this.dataSeveridad = data;
    });
  }

  getCatalogos() {
    this.catalogoService.getCatalogo().then((data: any) => {
      this.dataCatalogo = data;
    });
  }

  getServicios(tipo: any, catalogo: any) {
    this.servicioService.getServiciosByTipo(tipo, catalogo).subscribe(
      (data) => {
        this.dataServicio = data;
        console.log('cantidad de servicios: ' + data.length);
        if (this.dataServicio.length == 0) {
          this.utils.presentAlert(
            'No existen servicios disponibles para esta categoria'
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  seleccionarSeveridad( CodigoServeridad){
    let vseveridad;
    this.codSeveridad=CodigoServeridad;
     console.log(this.codSeveridad , " severidad")
     this.severidadService.getSeveridadPorNivelSeveridad(CodigoServeridad).then((data)=>{
      console.log(data)
      vseveridad = data;
      this.valorSevridad=vseveridad.valorSeveridad
      console.log(this.valorCritividad," , ",this.valorPrioridad," , ",  this.valorSevridad )
    })
    localStorage.setItem("codSeveridad",  CodigoServeridad);
    let codSeveridad = localStorage.getItem("codSeveridad");
    console.log("codigo de severidad seleccionado/guardado "+codSeveridad);
    for (const data of this.dataSeveridad) {
      if (codSeveridad === data.codSeveridad.toString()) {
        localStorage.setItem("nivelSeveridad", data.nivelSeveridad);
        this.nivelSeveridad = localStorage.getItem("nivelSeveridad");
       
      } else {
        this.nivelSeveridad = data.nivelSeveridad;
       
      }
    }

  }


  async createTicket() {
    let dataTicket: Ticket;
    let ticketStatus: any;
    let tiempoRespuestaSLA
    let valor
    tiempoRespuestaSLA=Math.trunc(this.valorCritividad*this.valorPrioridad*this.valorSevridad)
     if(this.obtenercodigoTicket !=null){
        valor =this.obtenercodigoTicket
     }else{
       valor=3
     }


    dataTicket = {
      codTicket: this.codTicket,
      codCatalogo: this.codCatalogo,
      codServicio: this.codServicio,
      descripcionTicket: this.descripcionTicket,
      codSeveridad: this.codSeveridad,
      codViaComunicacion: this.codViaComunicacion,
      codUsuario: this.codUsuario,
      sla: tiempoRespuestaSLA,
      url: this.imgUrl,
      fechaCreacion: this.fecha,
      estado: 1,
    };

    console.log(dataTicket);

    try {
      if (this.editable === '1') {
        ticketStatus = (await this.ticketService.updateTicket(dataTicket))
          .status;
          this.updateTicket()
      } else {
        ticketStatus = (await this.ticketService.createTicket(dataTicket))
          .status;
        this.addTicket();
      }
      console.log(ticketStatus + ' ticket status');
      if (ticketStatus === 200 || ticketStatus === 201) {
        if (this.editable === '1') {
          this.sendMsg = 'Ticket modificado exitosamente';
        } else {
          this.sendMsg = 'Ticket creado exitosamente';
        }
      } else {
        if (this.editable === '1') {
          this.sendMsg = 'Hubo un problema al tratar de moficar el ticket';
        } else {
          this.sendMsg = 'Hubo un problema al tratar de crear el ticket';
        }
      }
    } catch (error) {
      this.sendMsg = 'Error al tratar de crear o modificar el ticket ';
      console.log(
        JSON.stringify(error) + ' error al crear o moficar el ticket'
      );
    }
  }
}

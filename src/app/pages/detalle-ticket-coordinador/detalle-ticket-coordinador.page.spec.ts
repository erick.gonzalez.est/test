import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetalleTicketCoordinadorPage } from './detalle-ticket-coordinador.page';

describe('DetalleTicketCoordinadorPage', () => {
  let component: DetalleTicketCoordinadorPage;
  let fixture: ComponentFixture<DetalleTicketCoordinadorPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleTicketCoordinadorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetalleTicketCoordinadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

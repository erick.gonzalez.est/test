import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SolucionPage } from './solucion.page';

const routes: Routes = [
  {
    path: '',
    component: SolucionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SolucionPageRoutingModule {}
